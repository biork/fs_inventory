
# vim: ts=2:sw=2:sts=0:noet
from sys import stderr,exit,argv
from os import walk,stat,getcwd
from os.path import join as path_join,islink,isdir,isfile,dirname,basename
from re import compile as rx_compile,IGNORECASE
from datetime import datetime
import argparse
from hashlib import md5

_HEADER = '#partial-hash-len:'

# Output data line prefixes.
# Sections are separated by #-prefixed comment lines.

_SEARCH_PATH  = "RT"
_INCLUDE_FILE = "IF"
_EXCLUDE_FILE = "XF"
_EXCLUDE_DIR  = "XD"
_UNIQ_FILE    = "UF"
_DUP_FILE     = "DF"
_EXTENSION    = "FE"
_WASTED_BYTES = "BY"

_EVAL_RESULT_COUNTS = [
	"NM", # number matched
	"NI", # number failing include-file
	"NX", # number excluded by exclude-file
	"NR", # number in which stat throws OSError
	"NE"] # number of otherwise-matching files that are symlinks or empty


def _report( err ):
	print( err, file=stderr )


def _hash_bme( filepath, hashlen ) -> str:
	"""
	Hash either:
	1. the entire file, or
	2. hashlen bytes from the beginning, middle and end.
	defined byte spans in the head, middle and tail of filepath.
	This allows comparison of huge files without exhaustive I/O.
	"""
	filesize = stat( filepath ).st_size
	hasher = md5()
	with open( filepath, 'rb' ) as fp:
		if filesize <= 3*(hashlen+1): # +1 to preclude overlap, fwiw
			hasher.update( fp.read( filesize ) )
		else:
			hasher.update( fp.read( hashlen ) )
			fp.seek( (filesize // 2) - (hashlen // 2), 0 )
			hasher.update( fp.read( hashlen ) )
			try:
				fp.seek( filesize - hashlen, 0 )
			except OSError as e:
				print( e, filepath, filesize, file=stderr )
				exit(1)
			hasher.update( fp.read( hashlen ) )
	return hasher.hexdigest()


def _check( args ) -> bool:
	failures = 0
	with open(args.check) as fp:
		for line in fp:
			if line.startswith( _HEADER ):
				args.hashlen = int(line[len(_HEADER):].rstrip())
				continue
			# Lines should be _exactly_ like md5sum:
			# 32 ASCII chars, 2 spaces, file path.
			hash_expect = line[:32]
			target_path = line[34:].rstrip()
			hashes_match = False
			try:
				hash_actual = _hash_bme( target_path, args.hashlen )
			except (FileNotFoundError,PermissionError):
				print( "{}: {}".format( target_path, "FAILED open or read" ) )
			except:
				print( "{}: {}".format( target_path, "FAILED unknown reason" ) )
			else:
				hashes_match = (hash_expect == hash_actual)
				print( "{}: {}".format( target_path, "OK" if hashes_match else "FAILED" ) )
			if not hashes_match:
				failures += 1
	if failures > 0:
		print("WARNING:",failures,"computed checksum(s) did NOT match" )
	return failures == 0


def _filter_file( path, filename, db, args ) -> int:
	"""
	Store the path/filename in the database (dict) if it satisfies all
	requirements.
	"""
	result = 0
	fullpath = path_join( path, filename )
	if args.include_file and (not args.include_file.match( filename ) ):
		result = 1
	elif args.exclude_file and args.exclude_file.match( filename ):
		result = 2 
	try:
		stats = stat( fullpath )
	except:
		result = 3 if result == 0 else result
	if islink( fullpath ) or stats.st_size == 0:
		result = 4 if result == 0 else result

	if result != 0:
		if args.rejects:
			print( _EVAL_RESULT_COUNTS[result], fullpath, file=stderr )
		return result

	try:
		h = _hash_bme( fullpath, args.hashlen )
	except: # probably only PermissionError at this point(?)
		return 3

	if args.simple:
		print( "{}  {}".format(h,fullpath) )
	else:
		db.setdefault( h, [] ).append( ( path, filename, stats.st_size, stats.st_mtime ) )
	return 0


def _main( args ):
	eval_result_counts = [ 0 for _ in range(len(_EVAL_RESULT_COUNTS)) ]
	n_files = 0 # should account for _every file_ inspected!
	hashed = {}
	for root in args.roots:
		if isdir(root):
			for path,dirnames,filenames in walk( root, topdown=True, onerror=_report ):
				# In-place modification of dirnames is expressly allowed when
				# topdown==True, so it's used here to exclude directories matching
				# the --exclude_dir pattern.
				if args.exclude_dir:
					skip = [ (not args.exclude_dir.match(d) is None,i) for i,d in enumerate(dirnames) ]
					for m,i in reversed(skip):
						if m:
							if __debug__:
								print( "skipping directory:", dirnames[i], file=stderr)
							del dirnames[i]
				for filename in filenames:
					n_files += 1
					eval_result_counts[ _filter_file( path, filename, hashed, args ) ] += 1
		else:
			n_files += 1
			eval_result_counts[ _filter_file( dirname(root), basename(root), hashed, args ) ] += 1

	if args.simple:
		return # no further output.

	# Simply report files that are only present in one copy.

	print( "# unique files ({} ordinal fullpath size)".format(_UNIQ_FILE) )
	for k,l in hashed.items():
		# Check in this clause for hash collisions.
		if not all( t[2] == l[0][2] for t in l[1:] ):
			print( "Unexpected filesize differences for identical hashes:\n"
				+ "\n".join( path_join(t[0],t[1]) for t in l ), file=stderr )
		if len(l) == 1:
			p,f,s,_ = l[0]
			print( _UNIQ_FILE, '1', path_join(p,f), s, sep="\t" )

	# Files for which redundant copies exist get more extensive reporting.

	wasted_space = 0
	if any( len(v) > 1 for v in hashed.values() ):
		print( "# duplicate files ({} ordinal fullpath size mtime hash)".format(_DUP_FILE) )
	for k,l in hashed.items():
		if len(l) > 1:
			wasted_space += (len(l)-1)*l[0][2]
			l.sort( key=lambda t:t[0] ) # Sort by path
			#l.sort( key=lambda t:t[3] ) # Sort by modification time.
			for i,(p,f,s,m) in enumerate(l,1):
				mtime = datetime.fromtimestamp(m) # mtime.strftime("%Y%m%d")
				print( _DUP_FILE, i, path_join(p,f), s, mtime.isoformat(), k, sep="\t" )

	if wasted_space > 0:
		print( _WASTED_BYTES, wasted_space, "bytes in duplicated files", sep="\t" )

	for i in range(len(eval_result_counts)):
		print( _EVAL_RESULT_COUNTS[i], i, eval_result_counts[i], sep="\t" )
	print( "NT", sum(eval_result_counts),"/", n_files, sep="\t" )


############################################################################


parser = argparse.ArgumentParser(
	description="""\
Inventory all regular files under one or more root directories with
basenames satisfying given in/exclusion patterns and _efficiently_ identify
all duplicate files by _partial_ MD5 hashes.
""",
	formatter_class=argparse.RawDescriptionHelpFormatter,
	epilog="""
This is intended to identify potentially very large files that are
_potentially_ identical without hashing the entire file. Either:
1. the entire file, or
2. --hashlen bytes from the beginning, middle and end are hashed (MD5)
...to identify potentially identical files.
Regular expressions are:
1. applied only to _basenames_ of files and
2. must _match_ whole (base) filename.
""" )

# Input control

_VIDEO_PATTERN = r"^.*\.(png|jpg|jpeg|mp4)$"

parser.add_argument( "-i", "--include-file", metavar="REGEX",
	help="Regular expression defining files of interest." )
parser.add_argument( "--media", action="store_true",
	help="Use /{}/ as the include argument".format( _VIDEO_PATTERN ) )

parser.add_argument( "-x", "--exclude-file", metavar="REGEX",
	help="Regex describing file names to exclude" )
parser.add_argument( "-X", "--exclude-dir", metavar="REGEX",
	help="Regex describing directory names to exclude" )


parser.add_argument( "-s", "--simple", action="store_true",
	help="Output same as md5sum, without any further analysis." )

parser.add_argument( "-c", "--check", metavar="FILENAME",
	help="Verify hashes in the given %(metavar)s (generated by an earlier call with the -s option) match what is on disk." )

parser.add_argument( "-r", "--rejects", action="store_true",
	help="Emit rejected filepaths to stderr." )

parser.add_argument( "-l", "--hashlen", type=lambda v:int(v,base=0), default=0x4000, metavar="LENGTH",
	help="Number of bytes to hash at beginning, middle and end of file. [%(default)d]" )

parser.add_argument( "roots", default=[getcwd(),], metavar="DIRNAME", nargs="*",
	help="Directories to search for files. (May also include specific files to test.)" )

args = parser.parse_args()

if args.check:
	if isfile(args.check):
		exit( 0 if _check(args) else -1 )
	else:
		print( "File not found", args.check )
		exit(-1)

# Force hashlen to be even
if args.hashlen % 2 > 0:
	args.hashlen -= 1

if args.media:
	args.include_file = _VIDEO_PATTERN

if args.include_file:
	args.include_file = rx_compile( args.include_file )
if args.exclude_file:
	args.exclude_file = rx_compile( args.exclude_file )
if args.exclude_dir:
	args.exclude_dir = rx_compile( args.exclude_dir )

if not args.simple:
	print( "CL", *argv )
	print( "# search paths" )
	for i,d in enumerate(args.roots,1):
		print( _SEARCH_PATH, i, d, sep="\t" )
	print( "# inclusion/exclusion patterns" )
	if args.include_file:
		print( _INCLUDE_FILE, args.include_file.pattern, sep="\t" )
	if args.exclude_file:
		print( _EXCLUDE_FILE, args.exclude_file.pattern, sep="\t")
	if args.exclude_dir:
		print( _EXCLUDE_DIR, args.exclude_dir.pattern, sep="\t" )
else:
	# TODO: The presence of this header will NOT preclude the default md5sum
	# utility from working...and arriving at wrong conclusion!
	print( "{}{}".format( _HEADER, args.hashlen ) )

_main( args )

