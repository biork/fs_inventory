
from sys import stderr,argv
from os import getcwd,chdir,rename,walk
from os.path import splitext,join

DRY_RUN = len(argv) > 2 and argv[2].lower().find("dry") >= 0

IMAGE_EXTENSIONS = frozenset(['.tif','.gif','.jpg','.pnm','.png'])
VIDEO_EXTENSIONS = frozenset(['.mov','.avi','.mp4'])
for D,dl,fl in walk(argv[1]):
	if D != "." and not D.startswith("./tmp") and len(fl) > 0:
		lastdir = getcwd()
		chdir( D )
		print( "In", getcwd() )
		still = 1
		video = 1
		for f in sorted(fl):
			ext = splitext(f)[1].lower()
			if ext in IMAGE_EXTENSIONS:
				dest = "{:03d}{}".format(still, ext)
				if DRY_RUN:
					print( "> {} => {}".format( f, dest ) )
				else:
					rename( f, dest )
				still += 1
			elif ext in VIDEO_EXTENSIONS:
				dest = "{:03d}{}".format(video, ext)
				if DRY_RUN:
					print( "> {} => {}".format( f, dest ) )
				else:
					rename( f, dest )
				video += 1
			else:
				print( "skipping", join(D,f), file=stderr )
		chdir( lastdir )

