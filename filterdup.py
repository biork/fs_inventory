
from sys import stdin,stderr
from re import compile as rx_compile
import argparse

def _flush( accum, sig, args ):
    keep = set()
    for p in accum:
        if args.keep.match( p ):
            keep.add( p )
    if len(keep) == 1:
        remain = set(accum) - keep
        keeper = keep.pop()
        print( keeper, end="" )
        for p in remain:
            print( "\t{}".format(p), end="" )
        print()
    else:
        print( "# {} match(es) for /{}/ in...".format( len(keep), args.keep.pattern ) )
        for p in accum:
            print( "{}  {}".format( sig, p ), file=stderr )


# 1d8f750c510140b015d2021ee15599db  ./data/WGS/WGS_GEPARD/old_panel_seq_transfer/GE3309-rem/H52LGDRXY_GE3309-rem_S266_L001_R2_001.fastq.gz
# 1d8f750c510140b015d2021ee15599db  fastq/DNA/GEPARD/old_panel_seq_transfer/GE3309-rem/H52LGDRXY_GE3309-rem_S266_L001_R2_001.fastq.gz
# 953b8802b03a12c37a247e34e1adee8e  ./data/WGS/WGS_GEPARD/old_panel_seq_transfer/GE3309-rem/H52LGDRXY_GE3309-rem_S266_L001_R1_001.fastq.gz
# 953b8802b03a12c37a247e34e1adee8e  fastq/DNA/GEPARD/old_panel_seq_transfer/GE3309-rem/H52LGDRXY_GE3309-rem_S266_L001_R1_001.fastq.gz
# c965b9b3038670cb46652db5940c1495  ./data/WGS/WGS_GEPARD/old_panel_seq_transfer/GE3309-rem/H52LGDRXY_GE3309-rem_S266_L002_R2_001.fastq.gz
# c965b9b3038670cb46652db5940c1495  fastq/DNA/GEPARD/old_panel_seq_transfer/GE3309-rem/H52LGDRXY_GE3309-rem_S266_L002_R2_001.fastq.gz
def _main( fp, args ):
    sig = ""
    accum = []
    for line in fp:
        h = line[:32]
        p = line[34:].rstrip()
        if h != sig:
            if accum:
                _flush( accum, sig, args )
                accum = []
        sig = h
        accum.append( p )
    if accum:
        _flush( accum, sig, args )

############################################################################

parser = argparse.ArgumentParser(
    description="""\
Reformat a pre-sorted file of MD5 hashes (from md5sum) such that the file
to keep is in the first column.
""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog="""\
""" )

parser.add_argument( "-k", "--keep", required=True, metavar="REGEX",
    help="Regular expression to match the _one_ path among duplicates that should be kept." )
parser.add_argument( "inputs", nargs="*", metavar="FILEX",
    help="File(s) produced by md5sum." )

args = parser.parse_args()

args.keep = rx_compile( args.keep )

if args.inputs:
    for f in args.inputs:
        with open(f) as fp:
            _main( fp, args )
else:
    _main( stdin, args )

