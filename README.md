This utility has two primary purposes:
1. act as a stand-in replacement for md5sum for huge files
2. identify duplicates in one or more directory trees

In all cases it achieves its speed by only computing _partial_ hashes of
files. Specifically, it hashes a configurable number of bytes of the head,
middle and tail of each file.

It also provides for including and/or excluding files with regular
expressions.

It can also be run in "simple" mode which yields output that looks exactly
like md5sum, but is again, much faster (on huge files) because only parts
of the target files are hashed.

Obviously the strategy of partial hashing entails some risk, but for many
applications it is appropriate and on gigabyte+ files it is much faster than
md5sum.
