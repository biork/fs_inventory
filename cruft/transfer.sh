cut -f 2 dirs.tab | while read F; do
	D=${F%%/*}
	P=${F#*/}
	T="./d3/${P}"
	echo "${D} -- ${P} => ${T}"
	mkdir -p ${T}
	find "pic/$F" -maxdepth 1 -type f | while read X; do
		cp ${X} ${T}
	done
done

