
"""
sort -k 1,1 -k 2,2 pic/manifest.md5 |\
	awk 'BEGIN{last="";OFS="\t"}$1==last {next} {last=$1;print "k",$2}' |\
	sort -k 2,2 > nr-manifest.md5

pic/manifest.md5 had 9715 files
nr-manifest.md5 had 6601

yielding...

k	d01/2000_2006/4910remod/info/bathroom.csv
k	d01/2000_2006/4910remod/info/bathroom.dxf
k	d01/2000_2006/4910remod/info/bathroom.gnumeric
...
"""

from sys import argv,stdin,stdout,exit
from subprocess import run
from os.path import join,splitext,basename
import re

ROOT = argv[1]
DEST = argv[3]

def save( rec, path ):
	with open(path,"w") as fp:
		for r in rec:
			print( *r, sep="\t", file=fp )

def isImage( path ):
	return splitext(path)[1][1:].lower() in frozenset(['png','jpg','jpeg','gif'])

with open( argv[2] ) as fp:
	tag_image = [ line.rstrip().split('\t') for line in fp if not line.startswith("#") ]

filtering = True
patt = "d"
skip = re.compile(patt)

i = 0
while i < len(tag_image):

	tag  = tag_image[i][0]
	path = tag_image[i][1]

	if isImage( path ) and (not filtering or (skip is None) or (not skip.match( tag )) ):
		run( [ 'feh', '-F', '-Z', '--title', tag, join(ROOT,path) ] )
		response = input( "[{:04d}]{} {}({}) > ".format( i, "/{}/".format(patt) if filtering else "", basename(path), tag ) )
		n = len(response)
		if n > 0:
			if response[0] == "f":
				filtering = not filtering
				continue # without advancing to next image

			if response[0] == "F":
				try:
					regx = response[1:].rstrip()
					skip = re.compile(regx)
					patt = regx
				except re.error as e:
					print( e )
					skip = None
					patt = ""
				continue # without advancing to next image

			if response[0] == "<":        # backup [n] pics
				i -= ( int(response[1:]) if n > 1 else 1 )
				i = max(0,i)
				continue # without advancing to next image...obviously!

			if response[0] == "w":        # write the file immediately and optionally exit
				save( tag_image, DEST )
				if n > 1 and response[1] == "!":
					exit(0)
				continue # without advancing to next image

			tag_image[i][0] = response    # actually change the tag
		else:
			assert len(response) == 0     # leave the tag unchanged

	i += 1

save( tag_image, DEST )

